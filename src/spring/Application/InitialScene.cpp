#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));

		lineEditNrCurrent = new QLineEdit(centralWidget);
		lineEditNrCurrent->setObjectName(QStringLiteral("lineEditNrCurrent"));
		lineEditNrCurrent->setGeometry(QRect(60, 30, 271, 20));
		int intValue = boost::any_cast<int>(m_TransientDataCollection["MyValue"]);
		lineEditNrCurrent->setText(QString(std::to_string(intValue).c_str()));

		if (intValue != 1)
		{
			previousButton = new QPushButton(centralWidget);
			previousButton->setObjectName(QStringLiteral("previousButton"));
			previousButton->setGeometry(QRect(60, 180, 75, 23));
			previousButton->setText("Previous");
			QObject::connect(previousButton, SIGNAL(released()), this, SLOT(mf_previousButton()));
		}
		if (intValue != 1000)
		{
			nextButton = new QPushButton(centralWidget);
			nextButton->setObjectName(QStringLiteral("nextButton"));
			nextButton->setGeometry(QRect(260, 180, 75, 23));
			nextButton->setText("Next");
			QObject::connect(nextButton, SIGNAL(released()), this, SLOT(mf_nextButton()));
		}

		m_uMainWindow->setCentralWidget(centralWidget);
	}

	void InitialScene::mf_previousButton()
	{
		//In comentariu am folosit valorile salvate in m_TransientDataCollection
		//int intValue = boost::any_cast<int>(m_TransientDataCollection["MyValue"]);
		//m_TransientDataCollection.erase("MyValue");
		//m_TransientDataCollection.emplace("MyValue", intValue - 1);

		//Insa la urmatoarea varianta se tine cont de numarul aflat in lineEdit
		m_TransientDataCollection.erase("MyValue");
		m_TransientDataCollection.emplace("MyValue", lineEditNrCurrent->text().toInt() - 1);
		const std::string c_szNextSceneName = "BaseScene";
		emit SceneChange(c_szNextSceneName);
	}

	void InitialScene::mf_nextButton()
	{
		//In comentariu am folosit valorile salvate in m_TransientDataCollection
		//int intValue = boost::any_cast<int>(m_TransientDataCollection["MyValue"]);
		//m_TransientDataCollection.erase("MyValue");
		//m_TransientDataCollection.emplace("MyValue", intValue + 1);

		//Insa la urmatoarea varianta se tine cont de numarul aflat in lineEdit
		m_TransientDataCollection.erase("MyValue");
		m_TransientDataCollection.emplace("MyValue", lineEditNrCurrent->text().toInt() + 1);
		const std::string c_szNextSceneName = "BaseScene";
		emit SceneChange(c_szNextSceneName);

	}
}

